/*
 * serve file from cloud
 */

const { Storage } = require('@google-cloud/storage');
const path = require('path');
const { RTFMFail } = require('errors');

async function getFile (category, filename = 'index') {
   const storage = new Storage();

   try {
      const myBucket = storage.bucket(process.env.SRC_Bucket);
		const fileLocator = path.join(category, `${filename}.md`);
		console.log(`filepath is ${fileLocator}, from category: ${category} and filename: ${filename}`);
      const file = myBucket.file(fileLocator);

      const dataBuffer = await file.download();
		const [fileInfo] = await file.getMetadata();
		const returnObject = {
			id: filename,
			content: dataBuffer.toString()
		};
      return returnObject;
   }
   catch (err) {
      throw new RTFMFail(err.code, { values: [filename] });
   }
}

exports.getFile = getFile;
