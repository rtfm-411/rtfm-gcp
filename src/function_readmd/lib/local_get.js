/*
 * fetcher to fetch and deliver local files
 *
 */

const { readFile } = require('node:fs/promises');
const path = require('path');
const { RTFMFail } = require('errors');

const baseDir = path.resolve(process.cwd(), 'content');

function getFile (category, filename = 'index') {
	console.log(`getting file from ${baseDir} -- ${filename}`);
	return new Promise(async function (resolve, reject) {
		const fileLocator = path.resolve(baseDir, category, `${filename}.md`);
		console.log(`filepath is ${fileLocator}`);
		try {
			const content = await readFile(fileLocator, 'UTF-8');
			const returnObject = {
				id: filename,
				content
			};
			resolve(returnObject);
		}
		catch (error) {
			console.log('error')
			reject(new RTFMFail(error.code, { values: [baseDir, filename], cause: error }));
		}
	});
}

exports.getFile = getFile;
