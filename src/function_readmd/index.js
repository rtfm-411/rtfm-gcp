const { RTFMFail } = require('errors');

const isdev = process.env.RTFMDev || false;
const { getFile } = isdev ? require('./lib/local_get') : require('./lib/cloud_get');

// Should absolutely make this part of Error lib
function formatError (error) {
	error.content = {
		content: `# ${error.http_status}\n\nSeems something went wrong`
	};
}

async function readMD (req, res) {
	let content;

	// TODO: This is likely a very bad idea as it sets CORS
	// to allow all. This was done to work with the nginx proxy
	// which for some reason will not add these headers on status codes
	// greater than 400
   if (isdev) {
	   res.setHeader('Access-Control-Allow-Origin', '*');
	   res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
   }
	res.setHeader('X-Request-Time', Date.now());
	const [ category, page ] = req.path.match(/([a-z0-9\-]+)/g);
   console.log(`path: ${req.path}< category: ${category}< page: ${page}<`);
	try {
		content = await getFile(category, page);
	}
	catch (err) {
		let error = err;
		if (!err.http_status) {
			error = new RTFMFail(
				err.code,
				{
					values: [err.message]
				}
			);
		}
		formatError(error);
		return res.status(error.http_status).send(error.content);
	}

	return res.status(200).send(content);
}
exports.readMD = readMD;
