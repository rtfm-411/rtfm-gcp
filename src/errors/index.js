/*
 * Error formatter
 * should use standard Error type, message should contain:
 *  - Unique error code
 *  - Any needed details
 * 
 * Message, http response, should come from a common place
 */
const { format } = require('util');

const Errors = {
	404: {
		message: '%s The requested resource \'%s\' is not available.',
		http: 404
	},
	ENOENT: {
		message: '%s The requested resource \'%s\' is not available.',
		http: 404
	},
	UNK: {
		message: '%s An unknown error occurred: %s',
		http: 500
	}
};

class RTFMFail {
	constructor (code, options) {
		const error = Errors[code] || Errors.UNK;
		const cause = options.cause;
		this.code = code;
		this.http_status = error.http;
		this.message = format(error.message, ...([code, ...options.values] || []))
		this.error = new Error(this.message, { cause })
	}
}

exports.RTFMFail = RTFMFail;
