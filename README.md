# RTFM Project

Ultimately this project is to create an angular project that runs on google
cloud. Angular will be hosted as static with minimal backend interaction. The
backend will serve up markdown files from cloud storage.

## Requirements

Use of Cloud Functions, Cloud Storage, and Cloud Run.

Cloud run should be able to host static content from Cloud Storage (FE
Angular).

Cloud run will map endpoints to Cloud Functions which will read files from
Storage.

100% code coverage, automated, CI/CD.

## Testing

Current cloud function testing is only able to host a single function per a
single port. This makes it difficult to do testing with multiple functions.

Funny enough, I'm not sure I need more than one function. But it would be good
to have the ability.

### Docker for testing

Use docker-compose to setup an NGINX reverse proxy that connects to the
multiple functions.

#### Google Cloud Storage Testing

There currently exists no such framework to mock/test Cloud Storage. One way
would be to actually connect to Cloud Storage. That adds a level of complexity
and possible failure.

The function that interacts with Cloud Storage should set a environment
variable that indicates that it is a test environment. In this environment the
function should pull a file from local instead of from Cloud Storage.

It is not necessary for each MD file to be tested in this way, just basic
functionality.

#### Automation

This part may be interesting but is important as well.
